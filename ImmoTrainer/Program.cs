﻿using System;

namespace ImmoTrainer {
  class Program {
    private static double _budget = 0.0;
    private static double[] _huisPrijzen = new double[20];
    private static bool[] _huisIsGekocht = new bool[20];

    static void Main(string[] args) {
      Fase1();
      Fase2();
      Fase3();
    }

    private static void Fase3() {
      for (int i = 0; i < 20; i++) {
        double huisPrijs = _huisPrijzen[i];
        bool isGekocht = _huisIsGekocht[i];
        string msgIsGekocht = isGekocht ? "" : "niet ";
        Console.WriteLine("Huis met prijs {0} euro heb je {1}gekocht.",
          huisPrijs, msgIsGekocht);
      }
      double gemiddeldBetaald = BerekenGemiddelde(_huisIsGekocht, _huisPrijzen);
      Console.WriteLine($"Je spendeerde gemiddeld {gemiddeldBetaald} euro aan een huis.");
    }

    private static double BerekenGemiddelde(bool[] huisIsGekocht, double[] huisPrijzen) {
      double totaalUitgegeven = 0.0;
      int numGekocht = 0;
      for (int i = 0; i < huisPrijzen.Length; i++) {
        if (huisIsGekocht[i]) {
          totaalUitgegeven += huisPrijzen[i];
          numGekocht++;
        }
      }
      return totaalUitgegeven / Convert.ToDouble(numGekocht);
    }


    private static void Fase2() {
      int huisIndex = 0;
      while (_budget >= 1120 && huisIndex < 20) {
        int huisPrijs = HuisPrijs();
        _huisPrijzen[huisIndex] = huisPrijs;

        Console.WriteLine("Budget: {0}", _budget);
        Console.WriteLine("---------------");
        Console.Write("Wil je dit huis kopen? (j/n): ");
        char kopenOfni = Console.ReadLine().ToLower()[0];
        if (kopenOfni.Equals('j')) {
          _budget -= huisPrijs;
          _huisIsGekocht[huisIndex] = true;
        }

        huisIndex++;
      }
      
      while (huisIndex < 20) {
        _huisPrijzen[huisIndex] = HuisPrijs();
        huisIndex++;
      }
    }

    private static int HuisPrijs() {
      Random r = new Random();
      int numSlaapkamers = r.Next(1, 4);
      bool heeftTerras = r.Next(1, 4) == 1;
      int prijs = 1000 + numSlaapkamers * 120;
      string terrasMsg = "geen";
      if (heeftTerras) {
        prijs += 450;
        terrasMsg = "een";
      }
      Console.WriteLine("***-***-***");
      Console.WriteLine("Huis met {0} slaapkamers en {1} terras: {2} euro.",
        numSlaapkamers, terrasMsg, prijs);

      return prijs;
    }

    private static void Fase1() {
      Console.Write("Moeilijkheidsgraad? (1..9): ");
      int moeilijkheidsgraad = Convert.ToInt32(Console.ReadLine());
      Console.Write("Startbonus? (j/n): ");
      string inputStartbonus = Console.ReadLine().ToLower();
      bool krijgtStartbonus = inputStartbonus[0] == 'j' ? true : false;

      _budget = BudgetGenerator(moeilijkheidsgraad, krijgtStartbonus);
    }

    private static double BudgetGenerator(int moeilijkheidsgraad,
      bool krijgtStartbonus) {
      double result = 5000.0;
      if (krijgtStartbonus)
        result += 2500.0;
      if (moeilijkheidsgraad > 0)
        result -= moeilijkheidsgraad * 100.0;

      return result;
    }
  }
}